<?php

require_once('initialize.php');

$first_name = '';
$last_name = '';
$email = '';

if(isset($_POST['first_name'])) {
    $first_name = $_POST['first_name'];
}

if(isset($_POST['last_name'])) {
    $last_name = $_POST['last_name'];
}

if(isset($_POST['email'])) {
    $email = $_POST['email'];
}

function add_user($first_name, $last_name, $email) {
    
    $insert = 'insert into user (first_name, last_name, email, subscribed) values (:first_name, :last_name, :email, NOW())';
    
    $ins_stmt = DB::prepare($insert);
	
    DB::execute($ins_stmt,[':first_name' => $first_name, ':last_name' => $last_name, ':email' => $email]);
   
    
}

add_user($first_name, $last_name, $email);
header('Location: index.php');
exit();