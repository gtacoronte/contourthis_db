
DROP DATABASE IF EXISTS contourthis;
CREATE DATABASE contourthis;
USE contourthis;


CREATE TABLE user (
    id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    email VARCHAR(255),
    subscribed TIMESTAMP
);
-- 
-- CREATE TABLE item (
--     id INT auto_increment primary key,
--     name VARCHAR(100),
--     price DECIMAL(7,2)
-- );
-- 
-- CREATE TABLE invoice (
--     id INT AUTO_INCREMENT PRIMARY KEY,
--     customer_id INT NOT NULL,
--     created_at TIMESTAMP
-- );
-- 
-- CREATE TABLE invoice_item (
--     invoice_id INT NOT NULL,
--     item_id INT NOT NULL,
--     quantity INT,
--     PRIMARY KEY (`invoice_id`,`item_id`)
-- );



INSERT INTO user (first_name, last_name, email, subscribed)
    VALUES ('moe', "o'dools", 'moe@moes.com', NOW());
-- 
-- INSERT INTO customer (first_name, last_name, email, gender, customer_since)
--     VALUES ('g', 't', 'garrett@taco.com', 'male', CURDATE());
-- INSERT INTO customer (first_name, last_name, email, gender, customer_since)
--     VALUES ('garr', 'taco', 'garrett@taco.com', 'male', CURDATE());
-- INSERT INTO customer (first_name, last_name, email, gender, customer_since)
--     VALUES ('boss', 'tacor', 'garrett@taco.com', 'male', CURDATE());
-- 
-- INSERT INTO item (name, price) VALUES ('hammer', 17.95);
-- INSERT INTO item (name, price) VALUES ('dog from china', 0.30);
-- INSERT INTO item (name, price) VALUES ('screw driver', 13.99);
-- INSERT INTO item (name, price) VALUES ('box of nails', 9.99);
-- 
-- INSERT INTO invoice (customer_id, created_at) VALUES (1, NOW());
-- INSERT INTO invoice (customer_id, created_at) VALUES (2, NOW());
-- INSERT INTO invoice (customer_id, created_at) VALUES (1, NOW());
-- INSERT INTO invoice (customer_id, created_at) VALUES (4, NOW());
-- INSERT INTO invoice (customer_id, created_at) VALUES (3, NOW());
-- 
-- INSERT INTO invoice_item (invoice_id, item_id, quantity) VALUES (1, 1, 13);
-- INSERT INTO invoice_item (invoice_id, item_id, quantity) VALUES (2, 1, 5);
-- INSERT INTO invoice_item (invoice_id, item_id, quantity) VALUES (3, 3, 2);
-- INSERT INTO invoice_item (invoice_id, item_id, quantity) VALUES (4, 4, 8);
-- INSERT INTO invoice_item (invoice_id, item_id, quantity) VALUES (5, 4, 3);
-- INSERT INTO invoice_item (invoice_id, item_id, quantity) VALUES (5, 2, 2);
-- INSERT INTO invoice_item (invoice_id, item_id, quantity) VALUES (5, 1, 6);